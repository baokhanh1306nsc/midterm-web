const key = "c888ec9783814585d6bdd2c531205d31";
var baseUrl;
var posterSize;
const language = "en-US";
const homeUrl = `https://api.themoviedb.org/3/movie/top_rated?api_key=${key}&language=${language}`;
const searchUrl = `https://api.themoviedb.org/3/search/movie?api_key=${key}&language=${language}`;
const configUrl = `https://api.themoviedb.org/3/configuration?api_key=${key}`;
const movieUrl = `https://api.themoviedb.org/3/movie/`;
const personUrl = `https://api.themoviedb.org/3/search/person?api_key=${key}`;
const discoverUrl = `https://api.themoviedb.org/3/discover/movie?api_key=${key}&with_cast=`;
const personDetailUrl = `https://api.themoviedb.org/3/person/`;


async function getConfig() {
  const response = await fetch(configUrl);
  const config = await response.json();
  baseUrl = config.images.secure_base_url;
  posterSize = config.images.poster_sizes[config.images.poster_sizes.length - 1];
}

async function addPeople(url) {
  const response = await fetch(url);
  const people = await response.json();
  $('#main').empty();
  for (i in people.results) {
    if (people.results[i].known_for_department === "Acting") {
      var clickUrl = discoverUrl + people.results[i].id;
      $('#main').append(`
                    <div class="col-md-3 py-2">
                        <div class="card shadow h-100" onclick="paging(&quot;${clickUrl}&quot;)">
                            <img src="${baseUrl}${posterSize}${people.results[i].profile_path}" class="card-img-top img-thumbnail" alt="${people.results[i].name}">
                            <div class="card-body">
                                <h5 class="card-title">${people.results[i].name}</h5>
                            </div>
                        </div>
                    </div>
                `)
    }
  }
}


async function addMovie(url) {
  
  const response = await fetch(url);
  const movies = await response.json();
  $('#main').empty();
  for (let i = 0; i < movies.results.length; i++) {
    $('#main').append(`
                    <div class="col-md-3 py-2">
                        <div class="card shadow h-100" onclick="loadMovieDetailPage(${movies.results[i].id})">
                            <img src="${baseUrl}${posterSize}${movies.results[i].poster_path}" class="card-img-top img-thumbnail" alt="${movies.results[i].title}">
                            <div class="card-body">
                                <h5 class="card-title">${movies.results[i].title}</h5>
                                <p class="card-text">${movies.results[i].overview}</p>
                            </div>
                        </div>
                    </div>
            `)
  }
}

function loadMovieView(movies) {
  if (movies.results.length === 0) return;
  var carouselSlides = Math.floor(movies.results.length / 3) + 1;
  var lastCarouselItems = movies.results.length % 3;
  if (lastCarouselItems === 0) {
    lastCarouselItems = 3;
    carouselSlides -= 1;
  }
  var slideItems = new Array();
  for (let i = 1; i < carouselSlides; i++) {
    slideItems.push(3);
  }
  slideItems.push(lastCarouselItems);

  $('#main-container').append(`
  <div class="container my-4">
  <hr class="my-4">
  <h3 class="text-center">Movies</h3>
  <!--Carousel Wrapper-->
  <div id="multi-item-example" class="carousel slide carousel-multi-item" data-ride="carousel">

    <!--Controls-->
    <div class="row justify-content-center">
    <div class="controls-top">
      <a class="btn-floating" href="#multi-item-example" data-slide="prev"><i class="fa fa-chevron-left"></i></a>
      <a class="btn-floating" href="#multi-item-example" data-slide="next"><i class="fa fa-chevron-right"></i></a>
    </div>
  </div>
    <!--/.Controls-->

    <!--Indicators-->
    <ol class="carousel-indicators">
      <li data-target="#multi-item-example" data-slide-to="0" class="active"></li>
      <li data-target="#multi-item-example" data-slide-to="1"></li>
      <li data-target="#multi-item-example" data-slide-to="2"></li>
    </ol>
    <!--/.Indicators-->

    <!--Slides-->
    <div class="carousel-inner" role="listbox">

      <!--First slide-->
      <div class="carousel-item active w-100">
      <!--First row-->
          <div class="row" id="carousel-row-0">

          <div class="col-md-4">
          <div class="card mb-2 h-100" onclick="loadMovieDetailPage(${movies.results[0].id})" style="cursor: pointer;">
            <img class="card-img-top" src="${baseUrl}${posterSize}${movies.results[0].poster_path}"
              alt="${movies.results[0].title}">
            <div class="card-body">
              <h4 class="card-title">${movies.results[0].title}</h4>
              <p class="card-text text-justify">${movies.results[0].overview}</p>
            </div>
          </div>
        </div>        
      </div>
      <!--/.First row-->
      </div>
      <!--/.First slide-->

      </div>
      <!--/.Slides-->

    </div>
    <!--/.Carousel Wrapper-->


  </div>
  `);
  //first slide
  for (let i = 1; i < slideItems[0]; i++) {
    $('#carousel-row-0').append(`
    <div class="col-md-4 clearfix d-none d-md-block">
    <div class="card mb-2 h-100" onclick="loadMovieDetailPage(${movies.results[i].id})" style="cursor: pointer;">
      <img class="card-img-top" src="${baseUrl}${posterSize}${movies.results[i].poster_path}"
        alt="${movies.results[i].title}">
      <div class="card-body">
        <h4 class="card-title">${movies.results[i].title}</h4>
        <p class="card-text text-justify">${movies.results[i].overview}</p>
      </div>
    </div>
  </div>
`);
  }
  var movieIndex = 3;
  //other sildes
  for (let i = 1; i < carouselSlides; i++) {
    $('.carousel-inner').append(`
        <div class="carousel-item w-100">
        <!--First row-->
            <div class="row" id="carousel-row-${i}">

            <div class="col-md-4">
            <div class="card mb-2 h-100" onclick="loadMovieDetailPage(${movies.results[movieIndex * i].id})" style="cursor: pointer;">
              <img class="card-img-top" src="${baseUrl}${posterSize}${movies.results[i * movieIndex].poster_path}"
                alt="${movies.results[i * movieIndex].title}">
              <div class="card-body">
                <h4 class="card-title">${movies.results[i * movieIndex].title}</h4>
                <p class="card-text text-justify">${movies.results[i * movieIndex].overview}</p>
              </div>
            </div>
          </div>        
        </div>
        `);
  }
  for (let i = 1; i < carouselSlides; i++) {
    for (let j = 1; j < slideItems[i]; j++) {
      $('#carousel-row-' + i).append(`
      <div class="col-md-4 clearfix d-none d-md-block">
      <div class="card mb-2 h-100" onclick="loadMovieDetailPage(${movies.results[movieIndex * i + j].id})" style="cursor: pointer;">
        <img class="card-img-top" src="${baseUrl}${posterSize}${movies.results[movieIndex * i + j].poster_path}"
          alt="${movies.results[movieIndex * i + j].title}">
        <div class="card-body">
          <h4 class="card-title">${movies.results[movieIndex * i + j].title}</h4>
          <p class="card-text text-justify">${movies.results[movieIndex * i + j].overview}</p>
        </div>
      </div>
    </div>
      `);
    }
  }
}

async function loadPersonDetailPage(id) {
  loadingPage();
  const resMovies = await fetch(discoverUrl + id);
  const movies = await resMovies.json();
  const resDetail = await fetch(personDetailUrl + id + "?api_key=" + key + "&language=" + language);
  const detail = await resDetail.json();

  $('#main').empty();
  $('#main').append(`
<!-- Page Content -->
<div class="container" id="main-container">

  <!-- Portfolio Item Heading -->
  <h1 class="my-4">${detail.name}
  </h1>

  <!-- Portfolio Item Row -->
  <div class="row">

    <div class="col-md-5">
      <img class="img-thumbnail" src="${baseUrl}${posterSize}${detail.profile_path}" alt="${detail.name}">
    </div>

    <div class="col-md-7">
      <h3 class="my-3">Biography</h3>
      <p class="text-justify">${detail.biography}</p>
      <h3 class="my-3">Information</h3>
      <ul>
        <h5><li>Birthday: ${detail.birthday}.</li></h5>
        <h5><li>Place of birth: ${detail.place_of_birth}.</li></h5>
        </ul>
      </div>
    </div>
    </div>
    `);
  loadMovieView(movies);
}

async function paging(url) {
  if ($('.pagination').data("twbs-pagination")) {
    $('.pagination').twbsPagination('destroy');
  }
  loadingPage();
  const response = await fetch(url);
  const obj = await response.json();

  if (obj.total_results != 0) {
    $('.pagination').twbsPagination({
      totalPages: obj["total_pages"],
      visiblePages: 5,
      startPage: 1,
      onPageClick: function (event, page) {
        if (page != 1){
          loadingPage();
        }
        event.preventDefault();
        if (url.search("person") === -1) {
          addMovie(url + "&page=" + page);
        }
        else {
          addPeople(url + "&page=" + page);
        }
      }
    });
  }
  else {
    $('#main').empty();
    $('#main').append(`
        <h3 class="text-center w-100">Nothing</h3>
    `);
  }
}

function loadCastView(credits) {
  if (credits.cast.length === 0) return;
  var carouselSlides = Math.floor(credits.cast.length / 3) + 1;
  var lastCarouselItems = credits.cast.length % 3;
  if (lastCarouselItems === 0) {
    lastCarouselItems = 3;
    carouselSlides -= 1;
  }
  var slideItems = new Array();
  for (let i = 1; i < carouselSlides; i++) {
    slideItems.push(3);
  }
  slideItems.push(lastCarouselItems);

  $('#main-container').append(`
    <div class="container my-4">
    <hr class="my-4">
    <h3 class="text-center">Actors</h3>
    <!--Carousel Wrapper-->
    <div id="multi-item-example" class="carousel slide carousel-multi-item" data-ride="carousel" style="width: 100%;">

      <!--Controls-->
      <div class="row justify-content-center">
      <div class="controls-top">
        <a class="btn-floating" href="#multi-item-example" data-slide="prev"><i class="fa fa-chevron-left"></i></a>
        <a class="btn-floating" href="#multi-item-example" data-slide="next"><i class="fa fa-chevron-right"></i></a>
      </div>
    </div>
      <!--/.Controls-->

      <!--Indicators-->
      <ol class="carousel-indicators">
        <li data-target="#multi-item-example" data-slide-to="0" class="active"></li>
        <li data-target="#multi-item-example" data-slide-to="1"></li>
        <li data-target="#multi-item-example" data-slide-to="2"></li>
      </ol>
      <!--/.Indicators-->

      <!--Slides-->
      <div class="carousel-inner" role="listbox">

        <!--First slide-->
        <div class="carousel-item active w-100">
        <!--First row-->
            <div class="row" id="carousel-row-0">

            <div class="col-md-4">
            <div class="card mb-2 h-100" onclick="loadPersonDetailPage(${credits.cast[0].id})" style="cursor: pointer;">
              <img class="card-img-top" src="${baseUrl}${posterSize}${credits.cast[0].profile_path}"
                alt="${credits.cast[0].character}">
              <div class="card-body">
                <h4 class="card-title">${credits.cast[0].name}</h4>
                <p class="card-text">${credits.cast[0].character}</p>
              </div>
            </div>
          </div>        
        </div>
        <!--/.First row-->
        </div>
        <!--/.First slide-->

        </div>
        <!--/.Slides-->
  
      </div>
      <!--/.Carousel Wrapper-->
  
  
    </div>
    `);
  //first silde
  for (let i = 1; i < slideItems[0]; i++) {
    $('#carousel-row-0').append(`
            <div class="col-md-4 clearfix d-none d-md-block">
            <div class="card mb-2 h-100" onclick="loadPersonDetailPage(${credits.cast[i].id})" style="cursor: pointer;">
              <img class="card-img-top" src="${baseUrl}${posterSize}${credits.cast[i].profile_path}"
                alt="${credits.cast[i].character}">
              <div class="card-body">
                <h4 class="card-title">${credits.cast[i].name}</h4>
                <p class="card-text">${credits.cast[i].character}</p>
              </div>
            </div>
          </div>
        `);
  }
  //other slides
  var castIndex = 3;
  for (let i = 1; i < carouselSlides; i++) {
    $('.carousel-inner').append(`
          <div class="carousel-item w-100">
          <!--First row-->
              <div class="row" id="carousel-row-${i}">
              <div class="col-md-4">
              <div class="card mb-2 h-100" onclick="loadPersonDetailPage(${credits.cast[i * castIndex].id})" style="cursor: pointer;">
                <img class="card-img-top" src="${baseUrl}${posterSize}${credits.cast[i * castIndex].profile_path}"
                  alt="${credits.cast[i * castIndex].character}">
                <div class="card-body">
                  <h4 class="card-title">${credits.cast[i * castIndex].name}</h4>
                  <p class="card-text">${credits.cast[i * castIndex].character}</p>
                </div>
              </div>
            </div>        
          </div>
          `);
  }
  for (let i = 1; i < carouselSlides; i++) {
    for (let j = 1; j < slideItems[i]; j++) {
      $('#carousel-row-' + i).append(`
        <div class="col-md-4 clearfix d-none d-md-block">
        <div class="card mb-2 h-100" onclick="loadPersonDetailPage(${credits.cast[castIndex * i + j].id})" style="cursor: pointer;">
          <img class="card-img-top" src="${baseUrl}${posterSize}${credits.cast[castIndex * i + j].profile_path}"
            alt="${credits.cast[castIndex * i + j].character}">
          <div class="card-body">
            <h4 class="card-title">${credits.cast[castIndex * i + j].name}</h4>
            <p class="card-text">${credits.cast[castIndex * i + j].character}</p>
          </div>
        </div>
      </div>
        `);
    }
  }
}

async function loadMovieReview(url) {
  const response = await fetch(url);
  const reviews = await response.json();
  $('#main-reviews').empty();
  if (reviews.results.length === 0){
    $('#main-reviews').append(`
      <div class="col-fluid">
        <h4 class="text-center">Nothing</h3>
      </div>
    `)
  }
  for (i in reviews.results) {
    $('#main-reviews').append(`
      <div class="col-fluid">
      <h5><strong>Author: ${reviews.results[i].author}</strong></h3>
      <p class="text-justify">${reviews.results[i].content}</p>
      </div>
    `)
  }
}


async function loadMovieDetailPage(id) {
  loadingPage();
  $('.pagination').twbsPagination('destroy');
  const response = await fetch(movieUrl + id + "?api_key=" + key + "&language=" + language);
  const movie = await response.json();
  const creditsResponse = await fetch(movieUrl + id + "/credits" + "?api_key=" + key);
  const credits = await creditsResponse.json();
  var director;
  var genres = new Array();
  for (i in credits.crew) {
    if (credits.crew[i].job === "Director") {
      director = credits.crew[i].name;
    }
  }
  for (i in movie.genres) {
    genres.push(movie.genres[i].name);
  }
  $('#main').empty();
  $('#main').append(`
<!-- Page Content -->
<div class="container" id="main-container">

  <!-- Portfolio Item Heading -->
  <h1 class="my-4">${movie.title}
  </h1>

  <!-- Portfolio Item Row -->
  <div class="row">

    <div class="col-md-5">
      <img class="img-thumbnail" src="${baseUrl}${posterSize}${movie.poster_path}" alt="${movie.title}    ">
    </div>

    <div class="col-md-7">
      <h3 class="my-3">Overview</h3>
      <p class="text-justify">${movie.overview}</p>
      <h3 class="my-3">Information</h3>
      <ul>
        <h5><li>Release date: ${movie.release_date}.</li></h5>
        <h5><li>Director: ${director}.</li></h5>
        <h5><li id="genre">Genres: </li><h5>
        </ul>
      </div>
    </div>

    </div>
    `)
  for (i in genres) {
    $('#genre').append(`${genres[i]}`);
    if (i != genres.length - 1) {
      $('#genre').append(`, `);
    }
    else {
      $('#genre').append('.');
    }
  }
  loadCastView(credits);
  $('#main-container').append(`
      <div class="container my-4">
      <div class="row py-2">
        <hr>
      </div>
      <div class="row">
      <h3 class="text-center w-100">Reviews</h3>
      </div>
      <div class="row justify-content-md-center">
        <div id="main-reviews" class="row py-2">
          
        </div>
      </div>
      <div class="row justify-content-md-center">
      <nav aria-label="...">
          <ul class="pagination-review">
          </ul>
      </nav>
    </div>
    </div>
  `);
  loadMovieReview(movieUrl + id + "/reviews?api_key=" + key + "&language=" + language);
}

function loadingPage() {
  $('#main').empty();
  $('#main').append(`
  <div class="d-flex justify-content-center w-100">
  <div class="spinner-grow text-info" role="status">
  <span class="sr-only">Loading...</span>
</div>
</div>
    `);
}

async function loadHome() {
  paging(homeUrl);
}

async function onSearch(evt) {
  evt.preventDefault();
  var query = $("#query-submit").val();
  if (query == "") {
    alert("Form must be filled in");
  }
  else {
    if ($('#inputGroupSelect').val() === "Movie") {
      paging(searchUrl + "&query=" + query);
    }
    else {
      paging(personUrl + "&query=" + query);
    }
  }
  return true;
}
